# setup the GCP provider
terraform {
  required_version = ">= 0.12"
}
provider "google" {
  project     = "databento-docker-swarm"
  credentials = file("credentials.json")
  region      = "us-central1"
  zone        = "us-central1-a"
}