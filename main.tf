resource "google_service_account" "demo_service_account" {
  account_id   = "a4aj3185kka"
  display_name = "demo_service_account"
}

resource "google_compute_instance" "docker_swarm_main" {
  name         = "docker-swarm-main"
  machine_type = "e2-medium"
  zone         = "us-central1-a"
  tags         = ["databento"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
      labels = {
        env = "demo"
      }
      size = 20 # in GB
    }
  }

  network_interface {
    network = "default"
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.demo_service_account.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_instance" "docker_swarm_worker1" {
  name         = "docker-swarm-worker1"
  machine_type = "e2-medium"
  zone         = "us-central1-a"
  tags         = ["databento"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
      labels = {
        env = "demo"
      }
      size = 20 # in GB
    }
  }

  network_interface {
    network = "default"

  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.demo_service_account.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_instance" "docker_swarm_worker2" {
  name         = "docker-swarm-worker2"
  machine_type = "e2-medium"
  zone         = "us-central1-a"
  tags         = ["databento"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
      labels = {
        env = "demo"
      }
      size = 20 # in GB
    }
  }

  network_interface {
    network = "default"
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.demo_service_account.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_firewall" "rules" {
  project     = "databento-docker-swarm"
  name        = "databento-instances-web"
  network     = "default"
  description = "Creates firewall rule targeting tagged instances"

  allow {
    protocol  = "tcp"
    ports     = ["80", "8080", "1000-2000"]
  }

  source_tags = ["databento"]
}